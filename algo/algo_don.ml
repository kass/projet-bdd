open Data
open Index

(* Reste : proj, distinct, minus, restriction *)
let equal_array a1 a2 =
  let b = ref true in
  let n1 = Array.length a1 in
  let n2 = Array.length a2 in 
  if n1 != n2 then b:=false
  else
    for i=0 to (n1-1) do 
      if a1.(i) != a2.(i) then b:=false
    done;
    !b;;

let prod_cart t1 t2 = 
  let attributes_list1 = get_table_attributes t1 in
  let attributes_list2 = get_table_attributes t2 in
  let attributes_listf = attributes_list1 @ attributes_list2 in
  let attributes_type_list = List.(map get_attribute_type attributes_listf) in
  let table_prod = create_tmp_table attributes_type_list in
  let iter_1 = get_table_data t1  in
  let data_1 = ref (iter_1 ()) in
  while !data_1 != None do (
    let iter_2 = get_table_data t2 in
    let data_2 = ref (iter_2 ()) in
    while !data_2 != None do (
      match !data_1, !data_2 with
      | Some x, Some y -> insert_row table_prod (Array.append x y)
      | _ -> ();
      data_2 := iter_2 ());
    done;
    data_1 := iter_1 ());
  done;
  table_prod
;;

let proj table att =
  let prev_att = get_table_attributes table in
  let map = Array.make (List.length att) (-1) in
  List.iteri (fun i att -> map.(i) <- List.find_index (fun x -> x = att) prev_att |> Option.get) att;
  let attributes_type_list = List.map get_attribute_type att in
  let proj_table = create_tmp_table attributes_type_list in
  let iter = get_table_data table in
  let rec loop = function
    | Some d ->
      let new_data = Array.map (fun i -> d.(i)) map in
      insert_row proj_table new_data;
      loop (iter ())
    | _ -> ()
  in loop (iter ());
  proj_table

(* let proj table attribute_list =  *)
(*   let big_table_attributes_list = get_table_attributes table in *)
(*   let attribute_type_list = List.map get_attribute_type attribute_list in *)
(*   let proj_table = create_tmp_table attribute_type_list in *)
(*   let iter = get_table_data table in *)
(*   let data = ref (iter ()) in *)
(*   while !data != None do (  *)
(*     match !data with *)
(*     | Some d -> *)
(*       let new_data = Array.make (List.length attribute_list) d.(0) in *)
(*       let new_cursor = ref 0 in *)
(*       let old_cursor = ref 0 in *)
(*       let aux x = *)
(*         if !new_cursor < (List.length attribute_list) && x = List.nth attribute_list !new_cursor then *)
(*           ( *)
(*           new_data.(!new_cursor) <- d.(!old_cursor); *)
(*           new_cursor := !new_cursor + 1); *)
(*         old_cursor := !old_cursor + 1 in *)
(*       List.iter aux big_table_attributes_list; *)
(*       insert_row proj_table new_data; *)
(*       new_cursor := 0; *)
(*       old_cursor := 0; *)
(*     | _ -> ()); *)
(*     data := iter (); *)
(*      *)
(*   done; *)
(*   proj_table;; *)

let restriction table ope =
  let table_attributes_list = get_table_attributes table in
  let attribute_type_list = List.map get_attribute_type table_attributes_list in
  let restr_table = create_tmp_table attribute_type_list in
  let iter = get_table_data table in
  let data = ref (iter ()) in
  while !data != None do (
    match !data with
    | Some d ->
      if ope d then
        insert_row restr_table d
    | _ -> ());
    data := iter ();
  done;
  restr_table;;  




let join t1 t2 a1 a2 = 
  let pos1 = get_attribute_position a1 in
  let pos2 = get_attribute_position a2 + List.length (get_table_attributes t1) in  
  let t = prod_cart t1 t2 in (*les attributs *)
  let attributes_list = get_table_attributes t in
  let attributes_type_list = List.map get_attribute_type attributes_list in
  let join_table = create_tmp_table attributes_type_list in
  let iter = get_table_data t in
  let data = ref (iter ()) in
  while !data != None do (
    match !data with
    | Some d -> if d.(pos1) = d.(pos2) then insert_row join_table d
    | _ -> ();
    data := iter ());
  done;
  join_table;;



let compare_attributes_list_type a1 a2 =
  let at1 = List.map get_attribute_type a1 in
  let at2 = List.map get_attribute_type a2 in
  List.iter2 (fun x y -> if x <> y then raise (Invalid_argument "Les attributs n'ont pas le même type")) at1 at2;;


    

let union t1 t2 = 
  let attributes_list1 = get_table_attributes t1 in
  let attributes_list2 = get_table_attributes t2 in
  compare_attributes_list_type attributes_list1 attributes_list2;
  let attribute_type_list = List.map get_attribute_type attributes_list1 in
  let union_table = create_tmp_table attribute_type_list in
  let iter1 = get_table_data t1 in
  let data1 = ref (iter1 ()) in
  while !data1 != None do (
    match !data1 with
    | Some d -> insert_row union_table d
    | _ -> ();
    data1 := iter1 ());
  done;
  let iter2 = get_table_data t2 in
  let data2 = ref (iter2 ()) in
  while !data2 != None do (
    match !data2 with
    | Some d -> insert_row union_table d
    | _ -> ();
    data2 := iter2 ());
  done;
  union_table;;


let inter t1 t2 =
  let attributes_list1 = get_table_attributes t1 in
  let attributes_list2 = get_table_attributes t2 in
  compare_attributes_list_type attributes_list1 attributes_list2;
  let attribute_type_list = List.map get_attribute_type attributes_list1 in
  let inter_table = create_tmp_table attribute_type_list in
  let iter1 = get_table_data t1 in
  let data1 = ref (iter1 ()) in
  while !data1 != None do (
    let iter2 = get_table_data t2 in
    let data2 = ref (iter2 ()) in
    while !data2 != None do (
      match !data1, !data2 with
      | Some d1, Some d2 -> if equal_array d1 d2 then insert_row inter_table d1
      | _ -> ();
      data2 := iter2 ());
    done;
    data1 := iter1 ());
  done;
  inter_table;;






let distinct t =
  let attributes_list = get_table_attributes t in
  let attribute_type_list = List.map get_attribute_type attributes_list in
  let distinct_table = create_tmp_table attribute_type_list in
  let iter1 = get_table_data t in
  let element_table1 = ref (iter1 ()) in 
  while !element_table1 != None do
    let b = ref true in 
    let iter2 = get_table_data distinct_table in
    let element_table2 = ref (iter2()) in 
      while !element_table2 !=None do
        match !element_table1, !element_table2 with
        | Some x, Some y -> if (equal_array x y) then b:=false
        | _ -> ();
      done;
      if !b then 
        match !element_table1 with
        | Some x -> insert_row distinct_table x
        | _ -> ();
      done;
  distinct_table
;;

let minus table1 table2 = 
  let attributes_list = get_table_attributes table1 in
  let attribute_type_list = List.map get_attribute_type attributes_list in
  let minus_table = create_tmp_table attribute_type_list in
  let iter1 = get_table_data table1 in
  let element_table1 = ref (iter1()) in 
  while !element_table1 != None do
    let b = ref true in 
    let iter2 = get_table_data table2 in
      let element_table2 = ref (iter2()) in 
      while !element_table2 !=None do
        match !element_table1, !element_table2 with
        | Some x, Some y -> if (equal_array x y) then b:=false
        | _ -> ();
      done;
      if !b then 
        match !element_table1 with
        | Some x -> insert_row minus_table x
        | _ -> ();
      done;
  minus_table
;;





(*Si vous créez plusieurs algos pour une même opération, merci de les mettre dans ces listes*)
let union_list : (table->table->table) list = [union]
let inter_list : (table->table->table) list = [inter]
let minus_list : (table->table->table) list = [minus]
let prod_list : (table->table->table) list = [prod_cart]
let join_list : (table-> table -> attribute->attribute->table) list = [join]
let proj_list : (table -> attribute list -> table) list = [proj]
let restriction_list : (table -> (data array -> bool) -> table) list = [restriction]

open Index
open Data

val union : table->table->table
val inter : table->table->table
val minus : table->table->table
val prod_cart : table->table->table
val join: table-> table -> attribute->attribute->table
val proj: table -> attribute list -> table
val restriction : table -> (data array -> bool) -> table 
val distinct : table -> table


val union_list : (table->table->table) list
val inter_list : (table->table->table) list
val minus_list : (table->table->table) list
val prod_list : (table->table->table) list
val join_list : (table-> table -> attribute -> attribute -> table) list
val proj_list : (table -> attribute list -> table) list
val restriction_list : (table -> (data array -> bool) -> table) list

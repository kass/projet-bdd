(* on admet que :
- les formules sont bien typés (meme type de chaque côté de l'égalité)
- les tables / relations de la bdd ont un identifiant (= pointeur permettant l'accès aux tuples de la table)
- les unions et differences sont bien "union" compatible 
*)

(*
- c'est surement a nous d'implementer le type "formules" puisque on a besoin de les modifier dans l'optimisation
*)

(* ----- modules du groupe indexation ----- *)
open Data

module type Ident = sig
  type t
  val eq : t -> t -> bool

end


module type Attribut = sig
  module ID : Ident
  type t
  type formula =
    | True
    | Eq of ID.t * ID.t
    | And of formula * formula
    | Or of formula * formula

end

module type Table = sig
  module ID : Ident
  
  type t
  type metainfo

  val info : t -> metainfo
  val taille : metainfo -> int

  module ATTRIBUT : Attribut
  

end



(* ----- module du groupe algo ----- *)
type metainfo = Index.meta_info

module type Algo = sig
  module TABLES : Table 
  type t

  type algo_type =
  | PROJ_algo of TABLES.ATTRIBUT.ID.t list (*attribut list -> table -> table*)
  | RESTRICT_algo of TABLES.ATTRIBUT.formula (*formula -> table -> table*)
  | CROSS_algo  (*table -> table -> table*)
  | UNION_algo (*table -> table -> table*)
  | DIFF_algo  (*table -> table -> table*)
  | JOIN_algo of TABLES.ATTRIBUT.formula  (*formula -> table -> table -> table*)
  | OTHER


  val algo_list : algo_type -> t list
  val complexity : t -> metainfo -> int
  val table_info : TABLES.metainfo -> metainfo
  type map_id = (TABLES.ATTRIBUT.ID.t * TABLES.ATTRIBUT.t) list
  val eval1 : t -> (map_id * TABLES.t) -> (map_id * TABLES.t)
  val eval2: t -> (map_id * TABLES.t) -> (map_id * TABLES.t) -> (map_id * TABLES.t)

  val get_id_attribute : TABLES.ATTRIBUT.ID.t -> TABLES.ATTRIBUT.t
  val eval_formula : TABLES.ATTRIBUT.formula -> map_id -> Data.data Array.t -> bool 
end 


(* ----- module exposé ----- *)


module type Query = sig
  
end

module QUERY_OPTI (ALGO : Algo) =
struct

  type attribut = ALGO.TABLES.ATTRIBUT.ID.t
  type formula = ALGO.TABLES.ATTRIBUT.formula

  type table = ALGO.TABLES.t

  type algo = ALGO.t



  type query =
    | RELATION of (table * attribut list)
    | PROJ of query * attribut list
    | RESTRICT of query * formula
    | CROSS of query * query 
    | UNION of query * query 
    | DIFF of query * query 
    | JOIN of query * query * formula
    (*| RENAME of query * String * String *)

  (* ----- select descent ----- *)

  let rec clean (phi: formula) : formula =
    match phi with
    | And(True, f) | And(f, True) -> clean f
    | Or(True,f) | Or(f,True) -> clean f
    | And(f1,f2) -> And(clean f1, clean f2)
    | Or(f1,f2) -> Or(clean f1, clean f2)
    | _ -> phi

  let rec join_split (q: query) : query =
    match q with
    | JOIN (q1,q2,phi) ->
      begin match phi with
      | True -> CROSS(join_split q1, join_split q2)
      | And (f1,f2) -> RESTRICT(join_split (JOIN(join_split q1, join_split q2, f2)), f1)
      | Or(f1,f2) -> UNION(join_split (JOIN(join_split q1, join_split q2, f1)), join_split (JOIN(join_split q1, join_split q2, f2)))
      | Eq _ -> JOIN(join_split q1, join_split q2, phi)
      end
    | RELATION t -> RELATION t
    | PROJ (q1, l) -> PROJ(join_split q1, l)
    | RESTRICT (q1,phi) -> RESTRICT(join_split q1, phi)
    | CROSS (q1, q2) -> CROSS(join_split q1, join_split q2)
    | UNION (q1, q2) -> UNION(join_split q1, join_split q2)
    | DIFF (q1, q2) -> DIFF(join_split q1, join_split q2)
    | _ -> assert false


  (* theses two functions are mutualy recursive, the first one looks for "select", the second one cascade it the lower possible *)
  let rec select_descent (q : query) : query =
    match q with
    | RESTRICT (q', phi) -> select_cascade phi q' 
    (* otherwise just look for select *)
    | RELATION r -> RELATION r
    | PROJ (q', l) -> PROJ (select_descent q', l)
    | CROSS (q1, q2) -> CROSS (select_descent q1, select_descent q2)
    | UNION (q1, q2) -> UNION (select_descent q1, select_descent q2)
    | DIFF (q1, q2) -> DIFF (select_descent q1, select_descent q2)
    | JOIN (q1, q2, phi) -> JOIN (select_descent q1, select_descent q2, phi)

  and select_cascade (phi : formula) (q : query) : query = 
    match q with
    | RELATION r -> RESTRICT (RELATION r, phi)
    | RESTRICT (q', phi') -> select_cascade (And (phi, phi')) q'
    (* TODO for theses two cases we might be able to descent some part of the selection *)
    | CROSS (q1, q2) -> JOIN (select_descent q1, select_descent q2, phi)
    | JOIN (q1, q2, phi') -> JOIN (select_descent q1, select_descent q2, And (phi, phi'))
    (* other wise simply cascade the formula *)
    | PROJ (q', l) -> PROJ (select_cascade phi q', l)
    | UNION (q1, q2) -> UNION (select_cascade phi q1, select_cascade phi q2)
    | DIFF (q1, q2) -> DIFF (select_cascade phi q1, select_cascade phi q2)

  (* ----- projection descent ----- *)
  (* can be simplified using List module functions *)
  (* can add a more precise projection descent in the join and select *)

  let rec distinct_rev_append (l1 : 'a list) (l2 : 'a list) =
    match l1 with
    | [] -> l2
    | hd :: tl when List.mem hd l2 -> distinct_rev_append tl l2
    | hd :: tl -> distinct_rev_append tl (hd :: l2)

  let rec disjointed_list l g : bool =
    match l with
    | [] -> true
    | hd :: tl when List.mem hd g -> false
    | _ :: tl -> disjointed_list tl g

  let rec attribut_of_formula (phi : formula) : attribut list = 
    match phi with (* To modify *)
    | True -> []
    | Eq (a, b) -> [a; b]
    | And (phi1, phi2) -> (attribut_of_formula phi1) @ (attribut_of_formula phi2)
    | Or (phi1, phi2) -> failwith "TODO"

  let rec distinct_list l =
    match l with 
    | [] -> []
    | hd :: tl when List.mem hd tl -> distinct_list tl
    | hd :: tl -> hd :: (distinct_list tl)

  let rec projection_descent (q : query) : query =
    match q with
    | PROJ (q', l) -> projection_cascade q' l
    | RELATION _ | RESTRICT (RELATION _, _) -> q 
    | RESTRICT (q', phi) -> RESTRICT (projection_descent q', phi)
    | CROSS (q1, q2) -> CROSS (projection_descent q1, projection_descent q2)
    | UNION (q1, q2) -> UNION (projection_descent q1, projection_descent q2)
    | DIFF (q1, q2) -> DIFF (projection_descent q1, projection_descent q2)
    | JOIN (q1, q2, phi) -> JOIN (projection_descent q1, projection_descent q2, phi)

  (* attention cette fonction induit parfois des cas de projections sur des attributs qui existent pas *)
  and projection_cascade (q : query) (l1 : attribut list) : query =
    match q with
    | RELATION _ | RESTRICT (RELATION _, _) -> PROJ (q, l1)
    | PROJ (q', l2) -> projection_cascade q' l1 (* potentiellement des renommages à faire si les "attributs" donnent des numeros de colones *)
    | CROSS (q1, q2) -> CROSS (projection_cascade q1 l1, projection_cascade q2 l1)
    (* ici il faut séprarer les attributs qui viennent de droite et de gauche *)
    | JOIN (q1, q2, phi) -> (match disjointed_list l1 (distinct_list (attribut_of_formula phi)) with
      | true -> JOIN (projection_cascade q1 l1, q2, phi)
      | false -> PROJ (q, l1))
    | RESTRICT (q', phi) -> (match disjointed_list l1 (distinct_list (attribut_of_formula phi)) with
      | true -> RESTRICT (projection_cascade q' l1, phi)
      | false -> PROJ (q, l1))
    | UNION (q1, q2) -> UNION (projection_cascade q1 l1, projection_cascade q2 l1)
    | DIFF (q1, q2) -> PROJ (DIFF (q1, q2), l1) (* cannot go lower *)

  let query_optims : (query -> query) list = [select_descent; projection_descent]
    
  (* --------------------------------------------------------------------------------------------- *)
  (* ----- evaluation plan ----- *)
  (* --------------------------------------------------------------------------------------------- *)

  (* TODO il faut surement ajouter des fonctions de "sort" ou de "remove duplicate" *)

  (* un booleen sert à indiquer si on pipeline *)
  (*type eval_plan =
    | Table of table * ALGO.meta_inf
    | Operation of ALGO.algo_type * eval_plan * eval_plan option (* moche ? *) * ALGO.meta_inf * bool*)


  type eval_plan =
    | Leaf of ((table * attribut list) * metainfo)
    | Node1 of (eval_plan * algo * metainfo) 
    | Node2 of (eval_plan * eval_plan * algo * metainfo)

  let info_of_plan (e: eval_plan) : metainfo =
    match e with
    | Leaf (_,info) 
    | Node1(_,_,info) 
    | Node2(_,_,_,info) -> info

  let info_of_table (t: table) =
    ALGO.table_info (ALGO.TABLES.info t)

  let info_of_proj (m : metainfo) (l: attribut list) =
    { m with
    n_attributes = List.length l }

  let info_of_cross (m1 : metainfo) (m2 : metainfo) = 
    { m1 with
    n_tuples = m1.n_tuples * m2.n_tuples;
    n_attributes = m1.n_attributes + m2.n_attributes }

  let info_of_join (m1 : metainfo) (m2 : metainfo) (phi: formula) = 
    { m1 with
    n_tuples = m1.n_tuples * m2.n_tuples;
    n_attributes = m1.n_attributes + m2.n_attributes }

  let info_of_select (m : metainfo) (phi: formula) =
    m

  let info_of_union (m1 : metainfo) (m2 : metainfo) =
    { m1 with
    n_tuples = m1.n_tuples + m2.n_tuples }

  let info_of_diff (m1 : metainfo) (m2 : metainfo) = 
    m1

  let rec naive_plan (q: query) : eval_plan =
    match q with
    | RELATION t ->
        Leaf (t, info_of_table (fst t))
    | PROJ (q', l) -> 
        let e = naive_plan q' in
        let m = info_of_plan e in
        Node1 (e, List.hd (ALGO.algo_list (PROJ_algo l)), info_of_proj m l )
    | CROSS (q1, q2) ->
        let e1 = naive_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = naive_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, List.hd (ALGO.algo_list CROSS_algo), info_of_cross m1 m2)
    | JOIN (q1, q2, phi) ->
        let e1 = naive_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = naive_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, List.hd (ALGO.algo_list (JOIN_algo phi)), info_of_join m1 m2 phi)
    | RESTRICT (q', phi) ->
        let e = naive_plan q' in
        let m = info_of_plan e in
        Node1 (e, List.hd (ALGO.algo_list (RESTRICT_algo phi)), info_of_select m phi)
    | UNION (q1, q2) ->
        let e1 = naive_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = naive_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, List.hd (ALGO.algo_list UNION_algo), info_of_union m1 m2)
    | DIFF (q1, q2) ->
        let e1 = naive_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = naive_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, List.hd (ALGO.algo_list DIFF_algo), info_of_diff m1 m2)

  let pick_random () (l: 'a list) =
    Random.self_init ();
    let i = Random.int (List.length l) in
    let rec aux l' i' =
      match l' with
      | [] -> failwith "error"
      | hd::tl -> if i' = 0 then hd else aux tl (i' - 1)
    in aux l i

  let rec generate_plan (q: query)=
    match q with
    | RELATION t ->
        Leaf (t, info_of_table (fst t))
    | PROJ (q', l) -> 
        let e = generate_plan q' in
        let m = info_of_plan e in
        Node1 (e,  pick_random () (ALGO.algo_list (PROJ_algo l)), info_of_proj m l )
    | CROSS (q1, q2) ->
        let e1 = generate_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = generate_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, pick_random () (ALGO.algo_list CROSS_algo), info_of_cross m1 m2)
    | JOIN (q1, q2, phi) ->
        let e1 = generate_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = generate_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, pick_random () (ALGO.algo_list (JOIN_algo phi)), info_of_join m1 m2 phi)
    | RESTRICT (q', phi) ->
        let e = generate_plan q' in
        let m = info_of_plan e in
        Node1 (e, pick_random () (ALGO.algo_list (RESTRICT_algo phi)), info_of_select m phi)
    | UNION (q1, q2) ->
        let e1 = generate_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = generate_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, pick_random () (ALGO.algo_list UNION_algo), info_of_union m1 m2)
    | DIFF (q1, q2) ->
        let e1 = generate_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = generate_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, pick_random () (ALGO.algo_list DIFF_algo), info_of_diff m1 m2)


  let rec plan_complexity (e: eval_plan) : int =
    match e with
    | Leaf (t, info) -> ALGO.TABLES.taille (ALGO.TABLES.info (fst t))
    | Node1 (plan, alg, info) -> plan_complexity plan + ALGO.complexity alg info
    | Node2 (plan1, plan2, alg, info) -> plan_complexity plan1 + plan_complexity plan2 + ALGO.complexity alg info

  let find_plan (q: query) (timeout: int) : (eval_plan * int) =
  let plan = ref (naive_plan q) in
  let cost = ref (plan_complexity !plan) in
  for _ = 0 to timeout do
    let p = generate_plan q in
    let c = plan_complexity p in
    if c < !cost then (plan := p; cost := c)
  done;
  (!plan, !cost)



  let rec run (e: eval_plan) : (ALGO.map_id * table) =
    match e with
    | Leaf ((t, attr_l), _) -> (List.map (fun i -> (i, ALGO.get_id_attribute i)) attr_l, t)
    | Node1 (e1, alg, _) -> ALGO.eval1 alg (run e1)
    | Node2 (e1, e2, alg, _) -> ALGO.eval2 alg (run e1) (run e2)

end



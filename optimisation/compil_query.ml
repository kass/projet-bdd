open Query_opti
open Compil
open Index
open Data

module Id = struct
    type t = int
    let eq n p = (n = p)
end

module T = struct
    module ID = Id
    type metainfo = Index.meta_info
    type t = Index.table

    let info : t -> metainfo = fun table -> table_meta_info table
    let taille : metainfo -> int = fun info -> info.n_tuples

    module ATTRIBUT = struct
        module ID = Id
        type t = Index.attribute
        type formula =
          | True
          | Eq of ID.t * ID.t
          | And of formula * formula
          | Or of formula * formula
    end
end

(* ----------- just for the plot --------- *)
(* 
Index.create_table "table_de_test" [Index.create_attribute_type "attribut_test" Data.TINTEGER []];
*)
module ALG = struct
  module TABLES = T
  type metainfo = TABLES.metainfo

  type algo_type =
  | PROJ_algo of TABLES.ATTRIBUT.ID.t list (*attribut list -> table -> table*)
  | RESTRICT_algo of TABLES.ATTRIBUT.formula (*formula -> table -> table*)
  | CROSS_algo  (*table -> table -> table*)
  | UNION_algo (*table -> table -> table*)
  | DIFF_algo  (*table -> table -> table*)
  | JOIN_algo of TABLES.ATTRIBUT.formula  (*formula -> table -> table -> table*)
  | OTHER 

  type map_id = (TABLES.ATTRIBUT.ID.t * TABLES.ATTRIBUT.t) list

  (* Algorithms are provided with a function that remap the id to the new table generated *)
  type t =
    | NULLARY of TABLES.t
    | UNARY of ((map_id * TABLES.t) -> (map_id * TABLES.t))
    | BINARY of ((map_id * TABLES.t) -> (map_id * TABLES.t) -> (map_id * TABLES.t))
  
  let binary t1 t2 = Index.create_table "table_de_test" [create_attribute_type "id" TINTEGER [PRIMARY_KEY]]; Index.get_table "table_de_test"
  let unary t = Index.create_table "table_de_test" [create_attribute_type "id" TINTEGER [PRIMARY_KEY]]; Index.get_table "table_de_test"
  
  let map_id_hashtbl : (TABLES.ATTRIBUT.ID.t, TABLES.ATTRIBUT.t) Hashtbl.t = Hashtbl.create 5
  let get_id_attribute i =
    match Hashtbl.find_opt map_id_hashtbl i with
    | Some a -> a
    | None -> failwith "fail at compilation, ill-formed attributes"

  let rec eval_formula (f : TABLES.ATTRIBUT.formula) (map : map_id) (d : Data.data Array.t) : bool =
    match f with
    | True -> true
    | Eq (i1, i2) ->
      let a1 = List.assoc i1 map in
      let a2 = List.assoc i2 map in
      d.(Index.get_attribute_position a1) =: d.(Index.get_attribute_position a2)
    | And (f1, f2) ->
      let eval1 = eval_formula f1 map d in
      let eval2 = eval_formula f2 map d in
      eval1 && eval2
    | Or (f1, f2) ->
      let eval1 = eval_formula f1 map d in
      let eval2 = eval_formula f2 map d in
      eval1 || eval2

  let map_binder map = (fun x -> List.assoc x map)
  let remap_proj l _map new_t = List.combine l (Index.get_table_attributes new_t)
  let remap_restrict _phi map new_t = List.combine (fst (List.split map)) (Index.get_table_attributes new_t)
  let remap_cross map1 map2 new_t = List.combine (List.append (fst (List.split map1)) (fst (List.split map2))) (Index.get_table_attributes new_t)
  let remap_set_ope map1 map2 new_t = List.combine (fst (List.split map1)) (Index.get_table_attributes new_t)
  let remap_join _phi map1 map2 new_t = remap_cross map1 map2 new_t (* en attendant que join soit implémenter correctement :) *)

  let algo_list (alg_t: algo_type) : t list =
    match alg_t with
    | PROJ_algo l ->
        List.map
        (fun alg -> (UNARY (fun (map, t) -> let new_t = alg t (List.map (map_binder map) l) in (remap_proj l map new_t, new_t))))
        Algo_don.proj_list
    | RESTRICT_algo phi ->
        List.map
        (fun alg -> UNARY (fun (map, t) -> let new_t = alg t (fun d -> eval_formula phi map d) in (remap_restrict phi map new_t, new_t)))
        Algo_don.restriction_list
    | CROSS_algo ->
        List.map
        (fun alg -> BINARY (fun (map1, t1) (map2, t2) -> let new_t = alg t1 t2 in (remap_cross map1 map2 new_t, new_t)))
        Algo_don.prod_list
    | UNION_algo ->
        List.map
        (fun alg -> BINARY (fun (map1, t1) (map2, t2) -> let new_t = alg t1 t2 in (remap_set_ope map1 map2 new_t, new_t)))
        Algo_don.union_list
    | DIFF_algo ->
        List.map
        (fun alg -> BINARY (fun (map1, t1) (map2, t2) -> let new_t = alg t1 t2 in (remap_set_ope map1 map2 new_t, new_t)))
        Algo_don.minus_list
    | JOIN_algo phi ->
      begin match phi with
      | Eq (a1,a2) ->
        List.map
        (fun alg -> BINARY (fun (map1, t1) (map2, t2) -> let new_t = alg t1 t2 (List.assoc a1 map1) (List.assoc a2 map1) in
          (remap_join phi map1 map2 new_t, new_t)))
        Algo_don.join_list
      | _ -> failwith "shouldn't happen because of join_split"
      end
    | OTHER -> [] (*ne devrait pas arriver*)


  let complexity (alg: t) (m: metainfo) = 0
  let table_info info = info
  let eval1 alg table =
    match alg with
    | UNARY f -> f table
    | _ -> failwith "error"
  let eval2 alg t1 t2 =
    match alg with
    | BINARY f -> f t1 t2
    | _ -> failwith "error"


end

module OPTI = Query_opti.QUERY_OPTI(ALG)

(* -------------------- *)

let map_id_hastbl = ALG.map_id_hashtbl

type attribut = OPTI.attribut

type pre_query = Compil.query

type pre_formula = Compil.formula

let rec transform_formula (f : pre_formula) (m : (Compil.attribut * int) list) : OPTI.formula =
    match f with
    | Eq (a1, a2) -> Eq (List.assoc a1 m, List.assoc a2 m)
    | And (f1, f2) -> And (transform_formula f1 m, transform_formula f2 m)
    | Or (f1, f2) -> Or (transform_formula f1 m, transform_formula f2 m)

let attr_id = ref 0

let new_attr_id () =
    let id = !attr_id in
    incr attr_id; id
   
let assign_attr_from (table_name : string) (a : Index.attribute) =
    let id = new_attr_id () in
    Hashtbl.add ALG.map_id_hashtbl id a;
    ((table_name, Index.get_attribute_type_name (get_attribute_type a)), id)

let rec rename_attr (r : Compil.rename) (a : Compil.attribut) =
    match r with
    | [] -> a
    | (a', b) :: _ when a' = a -> b
    | _ :: r' -> rename_attr r' a

(* ATTENTION THIS FUNCTION DOES NOT WORK IF YOU DUPLICATE ATTRIBUTES IN PROJECTION *)
let transform (pq : pre_query) =
    let rec aux (q : pre_query) : ((Compil.attribut * int) list (* liste associative  *) * OPTI.query) =
    match q with
    | RENAME (q', r) ->
        let (m, transformed_q') = aux q' in
        (List.map (fun (a, i) -> (rename_attr r a, i)) m, transformed_q')
    | RENAME_TABLE (q', s) ->
        let (m, transformed_q') = aux q' in
        (List.map (fun ((t, a), i) -> ((s, a), i)) m, transformed_q') 
    | RELATION s ->
        let t = get_table s in
        let attr = get_table_attributes t in
        let map = List.map (assign_attr_from s) attr in (* liste de tout les noms d'attrbiuts de la table *)
        (map, OPTI.RELATION (t, snd (List.split map)))
    | PROJ (q', al) ->
        let (m, transformed_q') = aux q' in
        (m, OPTI.PROJ (transformed_q', List.map (fun a -> List.assoc a m) al))
    | SELECT (q', f) ->
        let (m, transformed_q') = aux q' in
        (m, OPTI.RESTRICT (transformed_q', transform_formula f m))
    | CROSS (q1, q2) ->
        let (m1, transformed_q1) = aux q1 in
        let (m2, transformed_q2) = aux q2 in
        (List.append m1 m2, OPTI.CROSS (transformed_q1, transformed_q2)) (* here we have to suppose names are distincts as expected *)
    | UNION (q1, q2) -> 
        let (m, transformed_q1) = aux q1 in
        let (_, transformed_q2) = aux q2 in (* here we have to suppose attributes are the same each sides as expected *)
        (m, OPTI.UNION (transformed_q1, transformed_q2))
    | DIFF (q1, q2) ->
        let (m, transformed_q1) = aux q1 in
        let (_, transformed_q2) = aux q2 in (* here we have to suppose attributes are the same each sides as expected *)
        (m, OPTI.DIFF (transformed_q1, transformed_q2))
    | JOIN (q1, q2, f) ->
        let (m1, transformed_q1) = aux q1 in
        let (m2, transformed_q2) = aux q2 in
        let m = List.append m1 m2 in
        (m, OPTI.JOIN (transformed_q1, transformed_q2, transform_formula f m))
    in aux pq


let rec pretty_print (q : OPTI.query) = match q with
  | RELATION t -> print_string "RELATION "; print_string "pas de nom de table"; print_string " "
  | PROJ (q', l) -> print_string"PROJ "; print_newline (); pretty_print q'
  | RESTRICT (q', phi) -> print_string "RESTRICT"; print_newline (); pretty_print q'; print_formula phi
  | CROSS (q1, q2)  -> print_string "CROSS"; print_newline (); pretty_print q1 ; print_newline (); pretty_print q2
  | UNION (q1, q2)  -> print_string "UNION"; print_newline (); pretty_print q1 ; print_newline (); pretty_print q2
  | DIFF (q1, q2) -> print_string "DIFF"; print_newline (); pretty_print q1 ; print_newline (); pretty_print q2
  | JOIN (q1, q2, phi) -> print_string "JOIN"; print_newline (); pretty_print q1 ; print_newline (); pretty_print q2; print_formula phi; print_string " "
  (*| RENAME of (q', s1, s2) -> print_string "RENAME"*)

  and print_formula (phi : OPTI.formula) = match phi with
| True -> print_string "True"
| Eq (a1,a2) -> print_string "TODO Eq"
| And (a1,a2) -> print_string "TODO And"
| Or (a1,a2) -> print_string "TODO Or"

and print_attribut (a : OPTI.attribut) = match a with
  | _ -> print_string "attribut"

and print_attribut_list (l : OPTI.attribut list) = match l with
  | [] -> ()
  | a :: tl -> print_attribut a; print_attribut_list tl

(* ---------- *)


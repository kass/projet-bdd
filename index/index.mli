open Data

type table
type attribute
type attribute_type
type constr =
  | PRIMARY_KEY
  | FOREIGN_KEY
  | UNIQUE

type meta_info = {n_tuples : int; n_attributes : int; attributes_size : int list; primary_index : attribute option}

(* TABLES *)
val create_table : string -> attribute_type list -> unit
val create_tmp_table : attribute_type list -> table
val get_table : string -> table
val get_table_name : table -> string
val get_table_data : table -> unit -> data array option
val get_table_attributes : table -> attribute list
(* val get_new_table_name : unit -> string *)


(* ATTRIBUTE TYPES *)
val create_attribute_type : string -> data_type -> constr list -> attribute_type
val get_attribute_type_name : attribute_type -> string
val get_attribute_type_data_type : attribute_type -> data_type
val get_attribute_type_constraints : attribute_type -> constr list
val update_attribute_type_display_name : attribute_type -> string -> unit

(* ATTRIBUTES *)
val insert_row : table -> data array -> unit
val get_attribute : table -> string -> attribute
val get_attribute_type : attribute -> attribute_type
val get_attribute_position : attribute -> int

(* METADATA *)
val table_meta_info : table -> meta_info

val print_table : table -> unit

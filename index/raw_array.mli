module type RawElt = sig
  type t

  val byte_len : int
  val default : t

  val write : bytes -> int -> t -> unit
  val read : bytes -> int -> t
end

module type S = sig
  type elt
  type t

  (** [length t] returns the number of elements in [t]. *)
  val length : t -> int
  val filename : t -> string

  (** [get t i] returns the [i]-th element of [t]. *)
  val get : t -> int -> elt

  (** [set t i x] sets the [i]-th element of [t] to [x]. *)
  val set : t -> int -> elt -> unit

  (** [make ?filename n] creates a new array of length [n]. If filename is not specified then the array is temporary *)
  val make : ?filename:(string option) -> int -> t

  (** [is_temporary t] returns true if [t] is temporary (ie. not persistent & in RAM if possible) *)
  val is_temporary : t -> bool

  (** [append t x] appends [x] to the end of [t]. *)
  val append : t -> elt -> unit
end

module Make (Elt : RawElt) : S with type elt = Elt.t

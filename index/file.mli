open Data

module type FileTable = sig

  type t (* Table sur le disk *)

  val create : string -> data_type list -> bool -> t

  val filename : t -> string
  val temporary : t -> bool

  val length : t -> int

  (** If None is returned, we don't have to recall the iterator *)
  val get_iterator : t -> unit -> data array option
  val append_data : t -> data array -> unit

end

module CSVTable : FileTable

module RawTable : FileTable

module type RawElt = sig
  type t

  val byte_len : int
  val default : t

  val write : bytes -> int -> t -> unit
  val read : bytes -> int -> t
end

module type S = sig
  type elt
  type t

  val length : t -> int
  val filename : t -> string
  val get : t -> int -> elt
  val set : t -> int -> elt -> unit
  val make : ?filename:(string option) -> int -> t
  val is_temporary : t -> bool
  val append : t -> elt -> unit
end

module Make (Elt : RawElt) = struct
  type elt = Elt.t
  type t = {
    tmp : bool;
    loaded_data : bytes;
    mutable len : int;
    filename : string;
    mutable fd : Unix.file_descr option;
  }

  let page_size = 4096
  let max_loaded_size = 100

  let default_filename =
    let counter = ref 0 in
    fun () -> incr counter; "array_" ^ (string_of_int !counter) ^ ".tmp"

  let length { len; _ } = len
  let filename { filename; _ } = filename

  let get t i =
    if i < 0 || i >= t.len then raise (Invalid_argument "Array.get")
    else
        if Option.is_some t.fd then (
          let fd = Option.get t.fd and offset = i * Elt.byte_len in
          Unix.lseek fd offset Unix.SEEK_SET |> ignore;
          Unix.read fd t.loaded_data 0 Elt.byte_len |> ignore;
          Elt.read t.loaded_data 0
        ) else Elt.read t.loaded_data (i * Elt.byte_len)


  let set t i v =
    if i < 0 || i >= t.len then raise (Invalid_argument "Array.set")
    else
      if Option.is_some t.fd then (
        let fd = Option.get t.fd and offset = i * Elt.byte_len in
        Elt.write t.loaded_data 0 v;
        Unix.lseek fd offset Unix.SEEK_SET |> ignore;
        Unix.write fd t.loaded_data 0 Elt.byte_len |> ignore
      ) else Elt.write t.loaded_data (i * Elt.byte_len) v

  let make ?(filename = None) n =
    let buf = Bytes.create (max_loaded_size * page_size) in
    let tmp = Option.is_none filename in
    let fname = match filename with
      | None -> default_filename ()
      | Some f -> f
    in
    let fd =
      if Option.is_some filename || n * Elt.byte_len > max_loaded_size * page_size then Some (Unix.openfile fname [Unix.O_RDWR; Unix.O_CREAT] 0o600)
      else None
    in
    let arr = { tmp; loaded_data = buf; len = n; filename = fname ; fd } in
    if Option.is_some fd && Sys.file_exists fname then
      let st = Unix.fstat (Option.get fd) in
      let len = st.st_size / Elt.byte_len in
      arr.len <- len
    else
      for i = 0 to n - 1 do
        set arr i Elt.default
      done;
    arr

  let is_temporary t = t.tmp

  let append t v =
    if Option.is_some t.fd || (t.len + 1) * Elt.byte_len >= max_loaded_size * page_size then (
      let fd =
        if Option.is_none t.fd then
          let fd = Unix.openfile t.filename [Unix.O_RDWR; Unix.O_CREAT] 0o600 in
          t.fd <- Some fd;
          Unix.write fd t.loaded_data 0 (t.len * Elt.byte_len) |> ignore;
          fd
        else Option.get t.fd
      in
      Elt.write t.loaded_data 0 v;
      Unix.lseek fd 0 Unix.SEEK_END |> ignore;
      Unix.write fd t.loaded_data 0 Elt.byte_len |> ignore;
      t.len <- t.len + 1
    ) else (
      Elt.write t.loaded_data (t.len * Elt.byte_len) v;
      t.len <- t.len + 1
    )

end

type data_type =
  | TVARCHAR of int (* = string *)
  | TINTEGER (* = int32 *)
  | TREAL (* = float *)
  | TBOOLEAN (* = bool *)
type data

exception Invalid_type
exception Invalid_comparaison of data * data

(* Data typs *)

val data_typ_of_string : string -> data_type
val data_typ_to_string : data_type -> string

(* Data *)

val (=:) : data -> data -> bool
val (<:) : data -> data -> bool

val varchar : int -> string -> data
val integer : int32 -> data
val real : float -> data
val boolean : bool -> data
val data_of_string : data_type -> string -> data
val string_of_data : data -> string

val get_string : data -> string
val get_int : data -> int32
val get_float : data -> float
val get_bool : data -> bool

(* Raw data *)
val data_type_size : data_type -> int

val make_data_raw_array : data_type list -> (module Raw_array.S with type elt = data array)

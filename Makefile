DUNE_PARAMS = --profile release

all:
	rm -f sgbd
	dune build @all $(DUNE_PARAMS)
	mv _build/default/bin/sgbd.exe sgbd

algo:
	dune build algo $(DUNE_PARAMS)

index:
	dune build index $(DUNE_PARAMS)

compil:
	dune build compil $(DUNE_PARAMS)

optimisation:
	dune build optimisation $(DUNE_PARAMS)

clean:
	dune clean
	rm -f sgbd metadata *.tbl *.tmp

.PHONY: all algo index compil optimisation clean

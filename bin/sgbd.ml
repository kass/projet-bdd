(* Il faut appeler la compil pour avoir la query, puis executer chez optim la query *)


let create_test_table () =
  let open Index in
  let att_typs = [create_attribute_type "id" TINTEGER [PRIMARY_KEY]; create_attribute_type "name" (TVARCHAR 16) []; create_attribute_type "size" TREAL []; create_attribute_type "online" TBOOLEAN []; create_attribute_type "note" TINTEGER []] in
  create_table "table" att_typs

let fill_table () =
  let open Index in
  let open Data in
  let tbl = get_table "table" in
  if (Index.table_meta_info tbl).n_tuples = 0 then (
    insert_row tbl [|integer 2l; varchar 16 "hugo"; real 2.1; boolean true; integer 2l|];
    insert_row tbl [|integer 4l; varchar 16 "simon"; real 1.3; boolean false; integer 20l|];
    insert_row tbl [|integer 6l; varchar 16 "sluko"; real 1.6; boolean true; integer 20l|];
    insert_row tbl [|integer 8l; varchar 16 "baptistou"; real 3.2; boolean false; integer 20l|]  
  )

let () =
  create_test_table ();
  fill_table ();
  if Array.length Sys.argv < 2 then failwith "please give a query >:(";
  let sql_query = Sys.argv.(1) in
  let query_opt = Compil.compile sql_query in
  match query_opt with
  | None -> failwith "something wrong"
  | Some q -> let (rename_id_map, refined_query) = Compil_query.transform q in
    (*Compil_query.pretty_print refined_query;*)
    let optimized_query = Compil_query.OPTI.select_descent (Compil_query.OPTI.projection_descent refined_query) in
    (*Compil_query.pretty_print optimized_query;*)
    let eval_plan = Compil_query.OPTI.find_plan optimized_query 1 in
    let (printed_attributes_map, result) = Compil_query.OPTI.run (fst eval_plan) in
    (* rename attributes *)
    let (renames, ids) = List.split rename_id_map in
    let id_rename_map = List.combine ids renames in
    List.iter (fun (i, a) -> Index.update_attribute_type_display_name (Index.get_attribute_type a) (snd (List.assoc i id_rename_map))) printed_attributes_map;
    Index.print_table result

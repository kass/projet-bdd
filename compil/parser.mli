(* Les types de bases pour le parsing *)

type word = char list (* Un mot classique sous forme de liste de caractères *)

type gchar = 
| T of char
| NT of string
type gword = gchar list

type rule = string*gword
type grammar = (string * (rule list))

type stree =
| C of char
| N of string*(stree list)
| Int of int
| LittWord of string

val parse : grammar -> string -> stree option
val print_stree : stree -> unit

val grammar_numbers : grammar
val number : gword
val numberNT : string

val grammar_littwords : grammar
val littword : gword
val littwordNT : string
